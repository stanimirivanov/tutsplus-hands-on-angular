# README #

This is a record of the [Hands-on Angular](http://courses.tutsplus.com/courses/hands-on-angular) course at [Tuts+](http://courses.tutsplus.com).

### 1. Introduction
1. Introduction
2. Setup Front End Application
3. Setup Backend Application

### 2. Public Area
1. Routes, Navigation and Directives
2. Unit Tests and E2E Tests With Protractor
3. Testing With Capybara
4. Display Edges on the Page
5. Filter Edges
6. Testing Boundaries of Two Applications
7. Acceptance Tests With Capybara and Factory Girl

### 3. User Section
1. Front-End Login Form and Validation
2. Token Authentication
3. Create New Edge
4. Refactor
5. Delete Edges
6. Conclusion

## Miscellaneous

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)