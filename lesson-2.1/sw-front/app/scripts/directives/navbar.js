'use strict';

/**
 * @ngdoc function
 * @name swFrontApp.directive:navbar
 * @description # navbar directive of the swFrontApp
 */
angular.module('swFrontApp').directive('navbar', function() {
	return {
		restrict: 'E',
		templateUrl: 'views/navbar.html',
		controller: 'NavigationController'
	};
});
